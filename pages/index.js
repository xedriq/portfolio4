import Layout from '../components/Layout'
import Introduction from '../components/Introduction'
import SubIntro from '../components/SubIntro'
import LetsWorkTogether from '../components/LetsWorkTogether'
import RecentWorks from '../components/RecentWorks'

const index = () => (
    <Layout>
        <Introduction />
        <SubIntro />
        <RecentWorks />
        <hr className="w-75" />
        <LetsWorkTogether />
    </Layout>
)

export default index