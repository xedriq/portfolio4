import { useState } from 'react'
import emailjs from 'emailjs-com';
import Swal from 'sweetalert2'

import Layout from '../components/Layout'

const getInTouch = () => {
	const [email, setEmail] = useState('')
	const [name, setName] = useState('')
	const [message, setMessage] = useState('')

	const handleChange = (e) => {
		let { id, value } = e.target

		if (id === "name") {
			setName(value)
		}

		if (id === "email") {
			setEmail(value)
		}

		if (id === "message") {
			setMessage(value)
		}
	}

	const handleSubmit = (e) => {
		e.preventDefault()

		let templateParam = {
			"reply_to": email,
			"from_name": name,
			"to_name": 'Xedriq',
			"message_html": message
		}

		emailjs.send('cedrick_tabares', 'template_drclsIFt', templateParam, 'user_xOqC0qhJmBYnsVu81we5g')
			.then((result) => {
				Swal.fire('I got your message. I\'ll get back to you as soon as possible. Thanks!')
					.then(() => {
						setName('');
						setEmail('');
						setMessage('');
						window.location.href = "https://xedriq.dev";
					})

			}, (error) => {
				console.log(error.text);
				Swal.fire({
					icon: 'warning',
					text: 'Oooopss.. something wen\'t wrong. Try again later.'
				})
			});
	}

	return < Layout >
		<div className="container my-5">
			<h1 align='center'>Get in touch</h1>
			<div className="row">
				<div className="col-12 col-md-6">
					<form onSubmit={handleSubmit} encType={'multipart/form-data'} id="contact-form">
						<div className="form-group">
							<label htmlFor="name" >Name</label>
							<input
								type="text"
								className="form-control"
								placeholder="Enter your name"
								id="name"
								onChange={handleChange}
								value={name} />
						</div>

						<div className="form-group">
							<label htmlFor="email" >Email</label>
							<input
								type="email"
								id="email"
								className="form-control"
								placeholder="Enter your email"
								onChange={handleChange}
								defaultValue={email} />
						</div>

						<div className="form-group">
							<label htmlFor="message" >Message</label>
							<textarea
								id="message"
								cols="65"
								rows="10"
								placeholder="Enter your message"
								className="form-control"
								onChange={handleChange}
								defaultValue={message}
							></textarea>
						</div>

						<button className="btn btn-primary">Send</button>

					</form>
				</div>
				<div className="col-12 col-md-6 my-4 my-md-0">
					<iframe
						id="gmap"
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1364.1933513930344!2d121.05594458954211!3d14.739625209267324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbe6df7554615ed54!2sPamahay%20Homes!5e0!3m2!1sen!2sph!4v1574235863856!5m2!1sen!2sph"
						frameBorder="0"
						style={{ border: 0 }}
						allowFullScreen=""
					></iframe>
				</div>
			</div>
		</div>

		<style jsx>{`
			#gmap {
				height: 100%;
				width: 100%;
			}

        	`}
		</style>
	</Layout >
}
export default getInTouch
