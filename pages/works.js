import Layout from '../components/Layout'
import Work from '../components/Work'
import LetsWorkTogether from '../components/LetsWorkTogether'


const works = () => (
	<Layout>
		<div className="container my-5">
			<h1 align="center" className="mb-5">my works</h1>
			<div className="row d-flex justify-content-center">
				<div className="col-12 col-md-4 my-4">
					<Work
						title="Booking System"
						pageLink="https://xedriq-pawtastic-frontend.herokuapp.com/"
						codeLink="https://gitlab.com/xedriq/cp3"
						image={require("../assets/images/pawtastic-ss.png")}
						techs={['html5', 'css3', 'js', 'bootstrap', 'reactjs', 'nodejs', 'graphql', 'express', 'heroku', 'mongodb']}
					/>
				</div>
				<div className="col-12 col-md-4 my-4">
					<Work
						title="Asset Management System"
						pageLink="https://reelhousestudio.xedriq.dev/"
						codeLink="https://gitlab.com/xedriq/cp2"
						image={require("../assets/images/capstone2.png")}
						techs={['html5', 'css3', 'js', 'php', 'laravel', 'SQL/MySQL', 'Google Cloud Platform']}
					/>
				</div>
				<div className="col-12 col-md-4 my-4">
					<Work
						title="E-commerce Design"
						pageLink='https://tuitt.gitlab.io/students/batch43/cedric-tabares/collectivedropdesign/'
						codeLink='https://gitlab.com/tuitt/students/batch43/cedric-tabares/collectivedropdesign'
						image={require('../assets/images/capstone1.png')}
						techs={['html5', 'css3', 'jquery', 'bootstrap']}
					/>
				</div>

				<div className="col-12 col-md-4 mt-4">
					<Work
						title="Portfolio"
						pageLink='https://xedriq.dev/'
						codeLink='https://gitlab.com/xedriq/portfolio4'
						image={require('../assets/images/portfolio-ss.png')}
						techs={['html5', 'css3', 'bootstrap', 'reactjs', 'nextjs', 'styled-components', 'zeit']}
					/>
				</div>

				<div className="col-12 col-md-4 mt-4">
					<Work
						title="Chat App"
						pageLink='https://xedriq-chat-app.herokuapp.com/'
						codeLink='https://github.com/xedriq/chat-app-nodejs'
						image={require('../assets/images/xedriq-chat-app.herokuapp.com_chat.png')}
						techs={['html5', 'css3', 'bootstrap', 'nodejs', 'express', 'socket.io', 'heroku']}
					/>
				</div>

			</div>

			<LetsWorkTogether />

		</div>
	</Layout>
)

export default works
