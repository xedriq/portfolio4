
const iconData = [
    {
        icons: [
            {
                name: 'HMTL5',
                // iconDiv: <i className='fab fa-html5'></i>
                iconDiv: <img src={require('./images/iconfinder_badge-html-5_317755.svg')} alt="" style={{ width: "70%" }} />
            },
            {
                name: 'CSS3',
                // iconDiv: <i className='fab fa-css3'></i>
                iconDiv: <img src={require('./images/iconfinder_badge-css-3_317756.svg')} style={{ width: "70%" }} />
            },
            {
                name: 'JavaScript',
                // iconDiv: <i className='fab fa-js'></i>
                iconDiv: <img src={require('./images/iconfinder_code-programming-javascript-software-develop-command-language_652581.svg')} alt="" style={{ width: "75%" }} />
            },
            {
                name: 'JQuery',
                iconDiv: <img src={require('./images/iconfinder_code-programming-javascript-jquery-develop-framework-language_652582.svg')} style={{ width: "75%" }} />
                // iconDiv: <img src="https://img.icons8.com/ios-filled/50/000000/jquery.png" />
            },
            {
                name: 'ReactJS',
                iconDiv: <img src={require('./images/iconfinder_sass.svg')} alt="" style={{ width: "70%" }} />
                // iconDiv: <i className='fab fa-react'></i>
            },
            {
                name: 'VueJS 2',
                iconDiv: <img src={require('./images/kisspng-vue-js.png')} alt="" style={{ width: "95%" }} />
                // iconDiv: <i className='fab fa-react'></i>
            },
            {
                name: 'Sass',
                iconDiv: <img src={require('./images/iconfinder_288_Sass_logo_4375066.svg')} alt="" style={{ width: "70%" }} />
                // iconDiv: <i className='fab fa-sass'></i>
            },
            {
                name: 'Bootstrap',
                iconDiv: <i className='fab fa-bootstrap' style={{ color: "#0275d8" }}></i>
            },
            {
                name: 'Bulma',
                iconDiv: <img src={require('./images/bulma-seeklogo.com.svg')} style={{ width: '50%' }} />
            },
            {
                name: 'Materialize',
                iconDiv: <img src={require('./images/materialize-logo.png')} style={{ width: '75%' }} />
            },

        ]
    },
    {
        icons: [
            {
                name: 'JavaScript',
                iconDiv: <img src={require('./images/iconfinder_code-programming-javascript-software-develop-command-language_652581.svg')} alt="" style={{ width: "75%" }} />
                // iconDiv: <i className='fab fa-js'></i>
            },
            {
                name: 'Node JS',
                // <div style={{ background:"lightgrey", borderRadius:"50%"}}>
                // </div>
                iconDiv: <img src={require('./images/iconfinder_nodejs-new-pantone-black_1012818.svg')} />
                // iconDiv: <i className='fab fa-node'></i>
            },
            {
                name: 'PHP',
                iconDiv: <img src={require('./images/iconfinder_php-logo_1012812.svg')} alt="" />
                // iconDiv: <i className='fab fa-php'></i>
            },
            {
                name: 'Laravel',
                iconDiv: <img src={require('./images/iconfinder_laravel_3069648.svg')} alt="" style={{ width: '70%' }} />
                // iconDiv: <i className='fab fa-laravel'></i>
            },
            {
                name: 'Express',
                iconDiv: <img src={require("./images/express-logo.png")} style={{ width: '75%' }} />
            },
            {
                name: 'GraphQL',
                iconDiv: <img src="https://img.icons8.com/color/50/000000/graphql.png" />
            },
            {
                name: 'MongoDB',
                iconDiv: <img src="https://img.icons8.com/color/48/000000/mongodb.png" />
            },
            {
                name: 'mySQL',
                iconDiv: <img src={require('./images/iconfinder_mysql_1322469.svg')} style={{ width: '75%' }} />
                // iconDiv: <img src="https://img.icons8.com/ios-filled/48/000000/sql.png" />
            },
            {
                name: 'Elasticsearch',
                iconDiv: <img src={require("./images/elasticsearch-logo.png")} style={{ width: '60%', color: 'grey' }} />
            },
            {
                name: 'Kibana',
                iconDiv: <img src={require("./images/kibana-logo.png")} style={{ width: '45%', color: 'grey' }} />
            },

        ]
    },
    {
        icons: [
            {
                name: 'NPM',
                iconDiv: <i className='fab fa-npm' style={{ color: 'lightgrey' }}></i>
            },
            {
                name: 'Yarn',
                iconDiv: <i className='fab fa-yarn' style={{ color: 'teal' }}></i>
            },
            {
                name: 'git',
                iconDiv: <i className='fab fa-git' style={{ color: 'lightgrey' }}></i>
            },
            {
                name: 'Linux',
                iconDiv: <img src={require('./images/iconfinder_logo_brand_brands_logos_linux_2993682.svg')} alt="" style={{ width: '75%' }} />
                // iconDiv: <i className='fab fa-linux'></i>
            },
            {
                name: 'Windows',
                iconDiv: <img src={require('./images/iconfinder_windows_317717.svg')} alt="" />
                // iconDiv: <i className='fab fa-windows'></i>
            },
            {
                name: 'Mac',
                iconDiv: <i className='fab fa-apple' style={{ color: 'lightgrey' }}></i>
            },
        ]
    }
]

export default iconData