import Link from 'next/link'
import styled from 'styled-components'
import { uuid } from 'uuidv4'

const ImageContainer = styled.div`
  background-image: url(${props => props.image});
  background-size: cover;
  background-position: center top;
  height: 230px;
  max-height: 230px;
  width: 100%;
`

const WorkContainer = styled.div`
  background-color: #ebebeb;
  width: 340px;
  border-radius: 3px;
  box-shadow: 5px 5px 20px rgba(0, 0, 0, 0.2);
  padding: 1em 1em;
  height: 100%;

  :hover {
      background-color: white;
      color: white;
  }
`

const Work = (props) => {
  let { title, image, pageLink, codeLink, techs } = props

  let badge = techs.map(tech => {
    return <span key={uuid()} className="badge badge-pill badge-primary align-text-bottom d-inline-block mx-1">{tech}</span>
  })

  return (
    <WorkContainer className="mb-4 mb-lg-0 mx-auto mx-lg-0 text-center">
      <h5 align='center'>{title}</h5>

      <hr className="w-75" />

      <ImageContainer image={image} />

      <Link href={pageLink} prefetch={false} >
        <a className="btn btn-primary btn-sm mx-3 my-3" target="_blank">Visit Page</a>
      </Link>

      <Link href={codeLink} prefetch={false}>
        <a className="btn btn-primary btn-sm mx-3 my-3" target="_blank">Check Code</a>
      </Link>
      <hr className="w-75" />

      {badge}

    </WorkContainer>
  )
}

export default Work