import Link from 'next/link'

const Footer = () => (
    <footer className="text-light bg-dark pt-5 pb-2 text-center">
        <div className="footer-info container-fluid d-md-flex justify-content-between">
            <div className="contact-info text-md-left">
                <h4 className="text-light">Cedrick Tabares</h4>
                <p>+63 9 266 681 1463</p>
                <p>B1 L33 Lou Sheff St. Pamahay Homes Pasong Putik Novaliches</p>
                <p>Quezon City Philippines 1118</p>
            </div>

            <div className="social text-md-right">
                <h4 className="text-light mb-4">Let's connect!</h4>
                <div className="d-md-flex justify-content-md-around">
                    <Link href="https://gitlab.com/xedriq" prefetch={false}>
                        <a target="_blank"><i className="fab fa-2x fa-gitlab text-light mr-3"></i></a>
                    </Link>
                    <Link href="https://github.com/xedriq" prefetch={false}>
                        <a target="_blank"><i className="fab fa-2x fa-github text-light mr-3"></i></a>
                    </Link>
                    <Link href="https://www.linkedin.com/in/cedricktabares" prefetch={false}>
                        <a target="_blank"><i className="fab fa-2x fa-linkedin text-light mr-3"></i></a>
                    </Link>
                    <Link href="https://www.facebook.com/xedriq" prefetch={false}>
                        <a target="_blank"><i className="fab fa-2x fa-facebook-square text-light"></i></a>
                    </Link>
                </div>
            </div>
        </div>

        <p className="copy mt-5">Cedrick Tabares &copy; {new Date().getFullYear()} </p>
    </footer>
);

export default Footer;
