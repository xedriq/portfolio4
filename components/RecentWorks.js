import Link from 'next/link'
import Work from './Work'

const RecentWorks = () => (
    <section className="works container my-5 text-center py-5">
        <h2 className="my-5 font-weight-bolder">Recent works</h2>
        <div className="row">
            <div className="col-12 col-lg-4 mt-4">
                <Work
                    title="Booking System"
                    pageLink="https://xedriq-pawtastic-frontend.herokuapp.com/"
                    codeLink="https://gitlab.com/xedriq/cp3"
                    image={require("../assets/images/pawtastic-ss.png")}
                    techs={['html5', 'css3', 'js', 'bootstrap', 'reactjs', 'nodejs', 'graphql', 'express', 'heroku', 'mongodb']}
                />
            </div>

            <div className="col-12 col-lg-4 mt-4">
                <Work
                    title="Asset Management System"
                    pageLink="https://reelhousestudio.xedriq.dev/"
                    codeLink="https://gitlab.com/xedriq/cp2"
                    image={require("../assets/images/capstone2.png")}
                    techs={['html5', 'css3', 'js', 'php', 'laravel', 'SQL/MySQL', 'Google Cloud Platform']}
                />
            </div>
            <div className="col-12 col-lg-4 mt-4">
                <Work
                    title="E-commerce Design"
                    pageLink='https://tuitt.gitlab.io/students/batch43/cedric-tabares/collectivedropdesign/'
                    codeLink='https://gitlab.com/tuitt/students/batch43/cedric-tabares/collectivedropdesign'
                    image={require('../assets/images/capstone1.png')}
                    techs={['html5', 'css3', 'jquery', 'bootstrap']}
                />
            </div>

        </div>




        <Link href='/works'>
            <a className="btn btn-primary px-5 py-3 my-5">
                All my works &rarr;
            </a>
        </Link>
    </section>

)

export default RecentWorks