import Link from 'next/link'

import styled from 'styled-components'
import Image from '../assets/images/hero-image.jpeg'

const HeroImage = styled.div`
    position: relative;
    background-image: url(${Image});
    background-size: cover;
    background-position: 50% 30%;
    background-repeat: no-repeat;
    width: 100%;
    height: 500px;
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    z-index: 100;
`

// const HeroContainer = styled.div`
//     width: 100%;
//     height: 500px;
//     border: 1px solid red;
//     overflow: hidden;
// `

const Introduction = () => (
    <section className="container mt-5 pt-5 text-center">
        <h1>
            I build designs and ideas into working web applications.
        </h1>
        <p className="w-75 mx-auto lead">
            Hi! I'm Cedrick Tabares, a full stack web developer from Quezon City, Philippines. With
            my recent intensive training at Zuitt Coding Bootcamp and professional experience as junior developer, I aim to help you in building your ideas into working
            web applications.
        </p>
        <Link href="/works">
            <a className="btn btn-primary px-5 py-3 my-5 ">
                My works &rarr;
            </a>
        </Link>

        <div className="row container-fluid mx-0 px-0 mt-5">
            <div className="col-md-10 mx-md-auto px-0">
                <HeroImage />
            </div>
            {/* <HeroContainer> */}
            {/* <img className="img-fluid" src={Image} alt="hero image" /> */}
            {/* </HeroContainer> */}
        </div>
    </section>
)

export default Introduction