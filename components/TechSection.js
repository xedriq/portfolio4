import styled from 'styled-components'
import { uuid } from 'uuidv4'

const IconSpan = styled.span`
	font-size: 3em;
`

const TechSection = (props) => {

	let iconDiv = props.iconData.icons.map(icon => {
		return (
			<div key={uuid()} className="col-3 text-center">
				<IconSpan>
					{icon.iconDiv}
				</IconSpan>
				<p className="text-light"><small>{icon.name}</small></p>
			</div>
		)
	})


	return (
		<>
			<h4 className="text-white text-center">{props.title}</h4>
			<div className="row">
				{iconDiv}
			</div>
		</>
	)
}

export default TechSection;