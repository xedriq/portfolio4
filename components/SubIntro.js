import Link from 'next/link'
import Technology from '../components/Technology'

const SubIntro = () => (
    <section className="container-fluid bg-dark py-5 main-content">
        <h2 className="container text-light mt-5">
            Let's create something awesome!
        </h2>

        <p className="container pb-5 text-light lead">
            Equipped with the knowledge on latest tools and technologies in web development, let's
            start building ideas into functional and dynamic websites!
        </p>


        <Technology />
        <h4 className="container my-5 text-center">
            <Link href="https://drive.google.com/file/d/1GQjkLJlkWV4Mo7-EG96uyCxx6yi2Y2Om/view?usp=sharing"
                prefetch={false}
            >
                <a target="_blank" className="text-white">Download my CV &rarr;</a>
            </Link>
        </h4>

        <style jsx>{`
        .main-content {
            position: relative;
        }


        .main-content::before {
            // border: 1px solid red;
            position: absolute;
            content: "";
            width: 100%;
            height: 15%;
            background: #343a40;
            
            top: -20%;
            left: 49.99%;
            transform: translate(-50%, 50%);
            z-index:-100;
        }
        `}</style>
    </section>

)

export default SubIntro