import Link from 'next/link'

const LetsWorkTogether = () => (
    <section className="lets-work container-fluid text-center py-5 my-5">
        <div className="container">
            <h5>Do you like my work?</h5>
            <h2 className="font-weight-bolder">Let's work together!</h2>
            <Link href="/get-in-touch">
	            <a className="btn btn-primary px-5 py-3 my-5">
	                Get in touch &rarr;
	        	</a>	
            </Link>
        </div>
    </section>
)

export default LetsWorkTogether