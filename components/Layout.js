import Head from 'next/head'
import Nav from './Nav'
import Footer from './Footer'

const Layout = (props) => (
    <div>
        <Head>
            <link rel="shortcut icon" href={require('../assets/images/myfavicon.png')} type="image/x-icon" />

            <script src="https://kit.fontawesome.com/109215626e.js" crossOrigin="anonymous"></script>

            <link rel="stylesheet" href="https://bootswatch.com/4/lux/bootstrap.min.css" />

            <title>Cedrick Tabares | Full Stack Web Developer</title>

        </Head>
        <Nav />
        {props.children}
        <Footer />


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossOrigin="true" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossOrigin="true" />
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossOrigin="true" />
        {/* font awesome */}
        <script src="https://kit.fontawesome.com/109215626e.js" crossOrigin="unknown"></script>
    </div>
)

export default Layout