import Link from 'next/link'


const Nav = () => (
    <nav className="navbar navbar-light sticky-top navbar-expand-md bg-light container-fluid">
       
        <Link href="/" >
            <a className="navbar-brand text-primary">Cedrick Tabares</a>
        </Link>

        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#my-navbar" aria-controls="my-navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="my-navbar">
            <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                    <Link href="/" >
                        <a className="nav-link text-primary">home</a>
                    </Link>
                </li>
                {/*
                <li className="nav-item">
                    <Link href="/about" >
                        <a className="nav-link text-primary">about me</a>
                    </Link>
                </li>
                */}
                <li className="nav-item">
                    <Link href="/works">
                        <a className="nav-link text-primary">my works</a>
                    </Link>
                </li>
                {/* <li className="nav-item">
                    <Link href="/hire-me">
                        <a className="nav-link text-primary">hire me</a>
                    </Link>

                </li> */}
                <li className="nav-item">
                    <Link href="/get-in-touch" >
                        <a className="nav-link text-primary">get in touch</a>
                    </Link>
                </li>
            </ul>
        </div>
    </nav>
)

export default Nav