import TechSection from './TechSection'
import iconData from '../assets/icondata'

const Technology = () => (
    <div className="container dev-tools mt-5">
        <h2 className="mb-4 text-light">Technologies I'm comfortable working with:</h2>
        <div className="tech-categories mt-5">
            <div className="row d-md-flex justify-content-md-between text-center">
                <div className="col-12 col-md-4 text-center px-4">
                    <TechSection title="Front End Development" iconData={iconData[0]} />
                </div>

                <div className="col-12 col-md-4 text-center px-4">
                    <TechSection title="Back End Development" iconData={iconData[1]} />
                </div>

                <div className="col-12 col-md-4 text-center px-4">
                    <TechSection title="Other Tools" iconData={iconData[2]} />
                </div>
            </div>
        </div>
    </div>
)

export default Technology